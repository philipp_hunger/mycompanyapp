import Foundation

class FeedbackType {
    var id: Int
    var name: String
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    func convertToDictionary() -> Dictionary<String, NSObject> {
        var feedbackTypeData = Dictionary<String, NSObject>()
        feedbackTypeData["id"] = self.id
        feedbackTypeData["name"] = self.name
        return feedbackTypeData
    }
    
    class func convertToFeedbackType(result: NSDictionary) -> FeedbackType {
        let id = result["id"] as! Int
        let name = result["name"] as! String
        return FeedbackType(id: id, name: name)
    }
}