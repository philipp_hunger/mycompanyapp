import Foundation

class Feedback {
    var id: Int
    var text: String
    var type: FeedbackType
    var emailAddress: String?
    
    init(id: Int, text: String, type: FeedbackType, emailAddress: String?) {
        self.id = id
        self.text = text
        self.type = type
        self.emailAddress = emailAddress
    }
    
    func convertToJson() -> String? {
        var feedbackData = Dictionary<String, NSObject>()
        feedbackData["text"] = text
        feedbackData["feedbackTypeId"] = type.id
        if (emailAddress != nil) {
            feedbackData["emailAddress"] = emailAddress!
        }
        var error: NSError?
        var jsonObject = NSJSONSerialization.dataWithJSONObject(feedbackData as NSDictionary, options: NSJSONWritingOptions.PrettyPrinted, error: &error)
        if (error != nil) {
            println("\(error?.localizedDescription)")
            return nil
        } else {
            return NSString(data: jsonObject!, encoding: NSUTF8StringEncoding) as! String?
        }
    }
}