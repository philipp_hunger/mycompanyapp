import Foundation

let kBundleName = NSBundle.mainBundle().infoDictionary!["CFBundleName"] as! String!

struct GlobalConstants {
    struct ApplicationInfo {
        static let applicationName = kBundleName
        static let applicationVersion = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as! String!
        static let baseUrl = NSBundle.mainBundle().infoDictionary!["RSApiBaseUrl"] as! String!
    }
    struct UserDefaultsKeys {
        static let reloadNewsOnShake = "\(kBundleName).reloadNewsOnShake"
        static let userEmailAddress = "\(kBundleName).userEmailAddress"
    }
    struct Font {
        static let family = "Helvetica"
        static let size = "12.0"
    }
}