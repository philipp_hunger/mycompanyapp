import UIKit
import CoreLocation

class PlacesViewController: UITableViewController, UISearchBarDelegate, CLLocationManagerDelegate, RestServiceApiProtocol {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    private let cellIdentifier = "PlaceCell"
    
    private let locationManager = CLLocationManager()
    
    private var location: CLLocation?
    
    private var places = [Place]()
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    private var filteredPlaces = [Place]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurePlacesTableViewController()
        RestServiceApi.getAllPlaces(self)
    }
    
    func configurePlacesTableViewController() {
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        searchBar.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func didReceiveData(data: NSData) {
        dispatch_async(dispatch_get_main_queue(), {
            var err: NSError?
            var jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
            if(err != nil) {
                println("Error while deserializing JSON: \(err!.localizedDescription)")
            } else {
                self.places = [Place]()
                let resultCount = jsonResult["resultCount"] as! Int
                if (resultCount > 0) {
                    let placeEntries = jsonResult["results"] as! NSArray
                    for entry in placeEntries {
                        if let userLocation = self.location {
                            self.places.append(Place.convertToPlaceWithDistance(entry as! NSDictionary, userLocation: userLocation))
                        } else {
                            self.places.append(Place.convertToPlace(entry as! NSDictionary))
                        }
                    }
                }
                self.places.sort({ $0.distance < $1.distance })
                self.filteredPlaces = self.places
            }
            self.tableView.reloadData()
        })
    }
    
    func didReceiveError(error: NSError) {
        dispatch_async(dispatch_get_main_queue(), {
            Utilities.showErrorAlert(self, message: error.localizedDescription)
        })
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: { (placemarks, error) -> Void in
            if (error != nil) {
                var errorMessage: String!
                switch error.code {
                case CLError.LocationUnknown.rawValue:
                    errorMessage = "Unkown location."
                case CLError.Denied.rawValue:
                    errorMessage = "Access to your location denied."
                case CLError.Network.rawValue:
                    errorMessage = "No network connection."
                default:
                    errorMessage = "Unable to determine your position."
                }
                Utilities.showErrorAlert(self, message: errorMessage)
            } else {
                let placemark = placemarks[0] as! CLPlacemark
                self.location = placemark.location as CLLocation
            }
            self.locationManager.stopUpdatingLocation()
            RestServiceApi.getAllPlaces(self)
        })
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Error while determining location.\nError message: \(error.localizedDescription)")
        var errorMessage: String!
        switch error.code {
        case CLError.LocationUnknown.rawValue:
            errorMessage = "Unkown location."
        case CLError.Denied.rawValue:
            errorMessage = "Access to your location denied."
        case CLError.Network.rawValue:
            errorMessage = "No network connection."
        default:
            errorMessage = "Unable to determine your position."
        }
        Utilities.showErrorAlert(self, message: errorMessage)
    }
    
    private func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if (buttonIndex == 1) {
            UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
        }
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredPlaces.count
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = self.tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! PlaceTableViewCell
        let place = filteredPlaces[indexPath.row]
        cell.nameLabel.text = place.name
        if let pictureContent = place.pictureContent {
            cell.placeImageView.image = UIImage(data: pictureContent)
        }
        return cell
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText.isEmpty) {
            filteredPlaces = places
        } else {
            filteredPlaces = places.filter( {$0.name.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil, locale: nil) != nil})
        }
        self.tableView.reloadData()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "PlaceSelected") {
            let placeViewController = segue.destinationViewController as! PlaceViewController
            placeViewController.place = filteredPlaces[tableView.indexPathForSelectedRow()!.row]
        }
    }
}
