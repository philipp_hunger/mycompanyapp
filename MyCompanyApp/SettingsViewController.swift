import UIKit

class SettingsViewController: UITableViewController  {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var shakeToUpdateNewsSwitch: UISwitch!
    
    @IBOutlet weak var emailAddressTextField: UITextField!
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSettingsViewController()
    }
    
    private func configureSettingsViewController() {
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        if let reloadNewsOnShake = NSUserDefaults.standardUserDefaults().boolForKey(GlobalConstants.UserDefaultsKeys.reloadNewsOnShake) as Bool? {
            shakeToUpdateNewsSwitch.on = reloadNewsOnShake
        }
        if let userEmailAddress = NSUserDefaults.standardUserDefaults().objectForKey(GlobalConstants.UserDefaultsKeys.userEmailAddress) as! String! {
            emailAddressTextField.text = userEmailAddress
        }
        saveButton.target = self
        saveButton.action = Selector("saveSettings")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func saveSettings() {
        NSUserDefaults.standardUserDefaults().setBool(shakeToUpdateNewsSwitch.on, forKey: GlobalConstants.UserDefaultsKeys.reloadNewsOnShake)
        if (emailAddressTextField.text == "") {
            NSUserDefaults.standardUserDefaults().removeObjectForKey(GlobalConstants.UserDefaultsKeys.userEmailAddress)
        } else {
            NSUserDefaults.standardUserDefaults().setObject(emailAddressTextField.text, forKey: GlobalConstants.UserDefaultsKeys.userEmailAddress)
        }
        let savedAlert = UIAlertController(title: "Settings", message: "Settings saved.", preferredStyle: UIAlertControllerStyle.Alert)
        savedAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in
            self.performSegueWithIdentifier("SettingsSaved", sender: alert)
        }))
        self.presentViewController(savedAlert, animated: true, completion: nil)
    }
}
