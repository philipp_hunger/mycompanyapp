import Foundation
import CoreLocation
import MapKit

class Place {
    var name: String
    var address: Address
    var phoneNumber: String
    var emailAddress: String
    var pictureContent: NSData?
    var openingHours: [OpeningHours]?
    var distance: String?
    
    init(name: String, address: Address, phoneNumber: String, emailAddress: String, pictureContent: NSData?, openingHours: [OpeningHours]?) {
        self.name = name
        self.address = address
        self.phoneNumber = phoneNumber
        self.emailAddress = emailAddress
        self.pictureContent = pictureContent
        self.openingHours = openingHours
    }
    
    class func convertToPlace(result: NSDictionary) -> Place {
        let name = result["name"] as! String
        let address = Address.convertToAddress(result["address"] as! NSDictionary)
        let phoneNumber = result["phoneNumber"] as! String
        let emailAddress = result["email"] as! String
        var pictureContent: NSData?
        if let pictureData = result["pictureContent"] as! String? {
            pictureContent = NSData(base64EncodedString: pictureData, options: NSDataBase64DecodingOptions.allZeros)
            
        }
        var openingHours: [OpeningHours]?
        if let openingHoursData = result["openingHours"] as? NSArray {
            openingHours = OpeningHours.convertToOpeningHours(openingHoursData)
        }
        var isOpen = result["isOpen"] as? Bool
        return  Place(name: name, address: address, phoneNumber: phoneNumber, emailAddress: emailAddress, pictureContent: pictureContent, openingHours: openingHours)
    }
    
    class func convertToPlaceWithDistance(result: NSDictionary, userLocation: CLLocation) -> Place {
        var place = Place.convertToPlace(result)
        let distanceFormatter = MKDistanceFormatter()
        distanceFormatter.units = MKDistanceFormatterUnits.Metric
        distanceFormatter.unitStyle = MKDistanceFormatterUnitStyle.Full
        place.distance = "\(distanceFormatter.stringFromDistance(userLocation.distanceFromLocation(place.address.location)))"
        return place
    }
    
    class func convertToPlaces(result: NSArray) -> [Place] {
        var places = [Place]()
        for entry in result {
            places.append(Place.convertToPlace(entry as! NSDictionary))
        }
        return places
    }
}