import UIKit
import Foundation

class Utilities {
    
    class func embedInHTMLStructure(title: String, content: String) -> String {
        var htmlContent = "<html><head><title>\(title)</title>"
        htmlContent += "<style> body { font-family: \(GlobalConstants.Font.family); font-size: \(GlobalConstants.Font.size); }</style></head>"
        htmlContent += "<body>\(content)</body></html>"
        return htmlContent
    }
    
    class func showErrorAlert(sender: UIViewController, message: String = "An unkown error occured.") {
        if (sender.presentedViewController == nil) {
            let alertController = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
            sender.presentViewController(alertController, animated: true, completion: nil)
        }
    }
}