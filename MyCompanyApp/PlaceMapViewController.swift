import UIKit
import MapKit

class PlaceMapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    var place: Place!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurePlaceMapViewController()
    }
    
    private func configurePlaceMapViewController() {
        self.title = place.name
        mapView.region = self.mapView.regionThatFits(MKCoordinateRegionMakeWithDistance(place.address.location.coordinate, 500, 500))
        let annotation = MKPointAnnotation()
        annotation.coordinate = place.address.location.coordinate
        annotation.title = place.name
        mapView.addAnnotation(annotation)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
