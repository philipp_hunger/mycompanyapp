import UIKit

class FeedbackViewController: UITableViewController, RestServiceApiProtocol {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var feedbackTypeCell: UITableViewCell!
    
    @IBOutlet weak var feedbackTextField: UITextField!
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var selectedFeedbackType: FeedbackType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureFeedbackViewController()
    }
    
    private func configureFeedbackViewController() {
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        saveButton.target = self
        saveButton.action = Selector("sendFeedback")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func didReceiveData(data: NSData) {
        dispatch_async(dispatch_get_main_queue(), {
            let savedAlert = UIAlertController(title: "Feedback", message: "Feedback succesfully sent.", preferredStyle: UIAlertControllerStyle.Alert)
            savedAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in
                self.performSegueWithIdentifier("FeedbackSaved", sender: alert)
            }))
            self.presentViewController(savedAlert, animated: true, completion: nil)
        })
    }
    
    func didReceiveError(error: NSError) {
        dispatch_async(dispatch_get_main_queue(), {
            Utilities.showErrorAlert(self, message: error.localizedDescription)
        })
    }
    
    @IBAction func selectFeedbackType(segue: UIStoryboardSegue) {
        let feedbackTypeTableViewController = segue.sourceViewController as! FeedbackTypesViewController
        if let feedbackType = feedbackTypeTableViewController.selectedFeedbackType {
            selectedFeedbackType = feedbackType
            setFeedbackTypeDetailText()
        }
    }
    
    private func setFeedbackTypeDetailText() {
        if let feedbackType = selectedFeedbackType {
            feedbackTypeCell.detailTextLabel?.textColor = UIColor.blackColor()
            feedbackTypeCell.detailTextLabel?.text = feedbackType.name
        }
    }
    
    func sendFeedback() {
        var valid = true
        let errorColor = UIColor.redColor()
        if (selectedFeedbackType == nil) {
            valid = false
            feedbackTypeCell.detailTextLabel?.textColor = UIColor.redColor()
        } else {
            setFeedbackTypeCellDetailText()
        }
        if (feedbackTextField.text.isEmpty) {
            valid = false
            setTextFieldErrorBorder(feedbackTextField)
        } else {
            resetTextFieldBorder(feedbackTextField)
        }
        if (valid) {
            let userEmailAddress = NSUserDefaults.standardUserDefaults().objectForKey(GlobalConstants.UserDefaultsKeys.userEmailAddress) as! String?
            let feedback = Feedback(id: 1, text: feedbackTextField.text, type: selectedFeedbackType!, emailAddress: userEmailAddress)
            RestServiceApi.saveFeedback(feedback, delegate: self)
        } else {
            let validationAlert = UIAlertController(title: "Feedback", message: "Please check your input.", preferredStyle: UIAlertControllerStyle.Alert)
            validationAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(validationAlert, animated: true, completion: nil)
        }
    }
    
    private func setFeedbackTypeCellDetailText() {
        if let feedbackType = selectedFeedbackType {
            feedbackTypeCell.detailTextLabel?.textColor = UIColor.blackColor()
            feedbackTypeCell.detailTextLabel?.text = feedbackType.name
        }
    }
    
    private func setTextFieldErrorBorder(textField: UITextField) {
        textField.layer.borderWidth = 1.0
        textField.layer.cornerRadius = 5
        textField.layer.borderColor = UIColor.redColor().CGColor
    }
    
    private func resetTextFieldBorder(textField: UITextField) {
        textField.layer.borderWidth = 0
        textField.layer.cornerRadius = 0
        textField.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "SelectFeedbackType") {
            if let feedbackType = selectedFeedbackType {
                let feedbackTypeTableViewController = segue.destinationViewController as! FeedbackTypesViewController
                feedbackTypeTableViewController.selectedFeedbackType = feedbackType
            }
        }
    }
}
