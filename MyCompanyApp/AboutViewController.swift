import UIKit

class AboutViewController: UITableViewController {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var applicationNameLabel: UILabel!
    
    @IBOutlet weak var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureAboutTableViewController()
    }
    
    private func configureAboutTableViewController() {
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        applicationNameLabel.text = GlobalConstants.ApplicationInfo.applicationName
        versionLabel.text = GlobalConstants.ApplicationInfo.applicationVersion
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
