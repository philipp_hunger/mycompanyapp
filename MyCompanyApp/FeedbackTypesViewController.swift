import UIKit

class FeedbackTypesViewController: UITableViewController, RestServiceApiProtocol {

    private let cellIdentifier = "FeedbackTypeCell"
    
    private var feedbackTypes = [FeedbackType]()
    
    var selectedFeedbackType: FeedbackType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RestServiceApi.getAllFeedbackTypes(self)
    }
    
    
    func didReceiveData(data: NSData) {
        dispatch_async(dispatch_get_main_queue(), {
            var err: NSError?
            var jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
            if(err != nil) {
                println("Error while deserializing JSON: \(err!.localizedDescription)")
            } else {
                self.feedbackTypes = [FeedbackType]()
                let resultCount = jsonResult["resultCount"] as! Int
                if (resultCount > 0) {
                    let newsEntries = jsonResult["results"] as! NSArray
                    for entry in newsEntries {
                        self.feedbackTypes.append(FeedbackType.convertToFeedbackType(entry as! NSDictionary))
                    }
                }
            }
            self.tableView.reloadData()
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        })
    }
    
    func didReceiveError(error: NSError) {
        dispatch_async(dispatch_get_main_queue(), {
            Utilities.showErrorAlert(self, message: error.localizedDescription)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedbackTypes.count
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let feedbackType = feedbackTypes[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! UITableViewCell
        cell.textLabel!.text = feedbackType.name
        
        if (selectedFeedbackType != nil) {
            if (feedbackType.id == selectedFeedbackType?.id) {
                cell.accessoryType = .Checkmark
            } else {
                cell.accessoryType = .None
            }
        } else {
            cell.accessoryType = .None
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if let feedbackType = selectedFeedbackType {
            var selectedFeedbackTypeIndex = 0
            while (feedbackTypes[selectedFeedbackTypeIndex].id != feedbackType.id) {
                selectedFeedbackTypeIndex++
            }
            let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: selectedFeedbackTypeIndex, inSection: 0))
            cell?.accessoryType = .None
        }
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = .Checkmark
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "SaveSelectedFeedbackType") {
            let cell = sender as! UITableViewCell
            let indexPath = tableView.indexPathForCell(cell)
            selectedFeedbackType = feedbackTypes[indexPath!.row]
        }
    }
}
