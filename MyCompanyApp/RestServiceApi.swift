import Foundation



protocol RestServiceApiProtocol {
    func didReceiveData(data: NSData)
    func didReceiveError(error: NSError)
}

var baseUrl: String {
    get {
        return "\(GlobalConstants.ApplicationInfo.baseUrl)/mycompany-restservice"
    }
}

class RestServiceApi {
    private class func performRequest(request: NSMutableURLRequest, delegate: RestServiceApiProtocol) {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        print("Performing request...")
        var dataTask = session.dataTaskWithRequest(request){
            (data: NSData!, response: NSURLResponse!, error: NSError!) -> Void in
            if let occuredError = error {
                println(" - An error occured while perfoming request: \(error.localizedDescription)")
                delegate.didReceiveError(occuredError)
            } else {
                let statusCode = (response as! NSHTTPURLResponse).statusCode
                if (statusCode == 200) {
                    println(" - Data received.")
                    delegate.didReceiveData(data)
                } else if (statusCode == 204) {
                    println(" - Successful.")
                    delegate.didReceiveData(data)
                } else {
                    println(" - Unsuccessful (status code \(statusCode))")
                    println("Descpription:\n\((response as! NSHTTPURLResponse).description)")
                    var userInfo: Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
                    userInfo["HTTP-Statuscode"] = statusCode
                    userInfo["Response"] = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    let httpError = NSError(domain: "\(NSBundle.mainBundle().bundleIdentifier!).RestServiceApi", code: statusCode, userInfo: userInfo)
                    delegate.didReceiveError(httpError)
                }
            }
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        dataTask.resume()
    }
    
    class func getAllNews(delegate: RestServiceApiProtocol) {
        let url = NSURL(string: baseUrl + "/news")!
        let request: NSMutableURLRequest  = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        RestServiceApi.performRequest(request, delegate: delegate)
    }
    
    class func getAllPlaces(delegate: RestServiceApiProtocol) {
        let url = NSURL(string: (baseUrl + "/place"))!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        RestServiceApi.performRequest(request, delegate: delegate)
    }
    
    class func getAllFeedbackTypes(delegate: RestServiceApiProtocol) {
        let url = NSURL(string: (baseUrl + "/feedback/type"))!
        var request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        RestServiceApi.performRequest(request, delegate: delegate)
    }
    
    class func saveFeedback(feedback: Feedback, delegate: RestServiceApiProtocol) {
        let url = NSURL(string: (baseUrl + "/feedback"))!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.setValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = feedback.convertToJson()?.dataUsingEncoding(NSUTF8StringEncoding)
        RestServiceApi.performRequest(request, delegate: delegate)
    }
}