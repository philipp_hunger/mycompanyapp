import Foundation

class News {
    var id: Int
    var title: String
    var categoryName: String
    var content: String
    var pictureContent: NSData?
    
    init(id: Int, title: String, categoryName: String, content: String, pictureContent: NSData?) {
        self.id = id
        self.title = title
        self.categoryName = categoryName
        self.content = content
        self.pictureContent = pictureContent
    }
    
    class func convertToNews(result: NSDictionary) -> News {
        let id = result["id"] as! Int
        let title = result["title"] as! String
        let categoryName = result["categoryName"] as! String
        let content = result["content"] as! String
        var pictureContent: NSData?
        if let pictureData = result["pictureContent"] as! String? {
            pictureContent = NSData(base64EncodedString: pictureData, options: NSDataBase64DecodingOptions.allZeros)
           
        }
        return News(id: id, title: title, categoryName: categoryName, content: content, pictureContent: pictureContent)
    }
}