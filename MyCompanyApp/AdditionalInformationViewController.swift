import UIKit

class AdditionalInformationViewController : UITableViewController, UITableViewDelegate {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureAdditionalInformationViewController()
    }
    
    private func configureAdditionalInformationViewController() {
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 0:
            UIApplication.sharedApplication().openURL(NSURL(string: "http://www.example.com")!)
        case 1:
            UIApplication.sharedApplication().openURL(NSURL(string: "http://www.facebook.com")!)
        case 2:
            UIApplication.sharedApplication().openURL(NSURL(string: "http://www.youtube.com")!)
        default:
            println("Invalid selection.")
        }
    }

}
