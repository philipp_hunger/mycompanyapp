import UIKit

class PlaceViewController: UITableViewController, UITableViewDelegate {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var placeInfoTextView: UITextView!
    
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var callButton: UIButton!
    
    @IBOutlet weak var sendMailButton: UIButton!
    
    @IBOutlet weak var openingHoursTableViewCell: UITableViewCell!
    
    @IBOutlet weak var openingHoursTextView: UITextView!
    
    var place: Place!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurePlaceViewController()
    }
    
    private func configurePlaceViewController() {
        callButton.addTarget(self, action: Selector("callPlace"), forControlEvents: UIControlEvents.TouchUpInside)
        sendMailButton.addTarget(self, action: Selector("sendMailToPlace"), forControlEvents: UIControlEvents.TouchUpInside)
        self.title = place.name
        placeInfoTextView.text = "\(place.name)\n\n\(place.address.street)\n\(place.address.postalCode), \(place.address.city)\n\(place.phoneNumber)\n\(place.emailAddress)"
        if let pictureContent = place.pictureContent {
            imageView.image = UIImage(data: pictureContent)
        }
        if let distanceFromUser = place.distance {
            distanceLabel.text = "\(distanceFromUser)"
        }
        var openingHoursText = ""
        if let openingHours = place.openingHours {
            for openingHour in openingHours {
                openingHoursText += "\(openingHour.weekday): \(openingHour.fromTime) - \(openingHour.untilTime)\n"
            }
        }
        openingHoursTextView.text = openingHoursText
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func callPlace() {
        println("Calling \(place.phoneNumber)...")
        UIApplication.sharedApplication().openURL(NSURL(string: "telprompt://\(place.phoneNumber)")!)
    }
    
    func sendMailToPlace() {
        println("Send mail to \(place.emailAddress)...")
        UIApplication.sharedApplication().openURL(NSURL(string: "mailto://\(place.emailAddress)")!)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "ShowPlaceOnMap") {
                (segue.destinationViewController as! PlaceMapViewController).place = place
        }
    }
}
