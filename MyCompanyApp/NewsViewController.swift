import UIKit
import AudioToolbox

class NewsViewController: UIViewController, RestServiceApiProtocol {

    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var contentView: UIWebView!
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var shareButton: UIBarButtonItem!
    
    private var newsEntries: [News]!
    
    private var selectedNews: News?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNewsViewController()
        loadNews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func configureNewsViewController() {
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        shareButton.target = self
        shareButton.action = "showActivityViewController"
        pageControl.addTarget(self, action: "setContentForContentView", forControlEvents: .ValueChanged)
    }
    
    private func loadNews() {
        pageControl.hidden = true
        contentView.hidden = true
        imageView.hidden = true
        RestServiceApi.getAllNews(self)
    }
    
    func didReceiveData(data: NSData) {
        dispatch_async(dispatch_get_main_queue(), {
            var err: NSError?
            var jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &err) as! NSDictionary
            if(err != nil) {
                println("Error while deserializing JSON: \(err!.localizedDescription)")
                Utilities.showErrorAlert(self)
            } else {
                self.newsEntries = [News]()
                let resultCount = jsonResult["resultCount"] as! Int
                if (resultCount > 0) {
                    let newsEntries = jsonResult["results"] as! NSArray
                    for entry in newsEntries {
                        let id = (entry as! NSDictionary)["id"] as! Int
                        self.newsEntries.append(News.convertToNews(entry as! NSDictionary))
                    }
                    self.pageControl.numberOfPages = self.newsEntries.count
                    self.pageControl.currentPage = 0
                    self.selectedNews = self.newsEntries[self.pageControl.currentPage]
                    if let pictureContent = self.selectedNews!.pictureContent {
                        self.imageView.image = UIImage(data: pictureContent)
                    }
                    self.contentView.loadHTMLString(Utilities.embedInHTMLStructure(self.selectedNews!.title, content: self.selectedNews!.content), baseURL: nil)
                    self.pageControl.hidden = false
                    self.contentView.hidden = false
                    self.imageView.hidden = false
                }
            }
        })
    }
    
    func didReceiveError(error: NSError) {
        dispatch_async(dispatch_get_main_queue(), {
            Utilities.showErrorAlert(self, message: error.localizedDescription)
        })
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent) {
        if motion == .MotionShake {
            if let reloadNewsOnShake = NSUserDefaults.standardUserDefaults().boolForKey(GlobalConstants.UserDefaultsKeys.reloadNewsOnShake) as Bool? {
                if (reloadNewsOnShake) {
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                    loadNews()
                }
            }
        }
    }
    
    func setContentForContentView() {
        selectedNews = newsEntries[pageControl.currentPage]
        if let news = selectedNews {
            if let pictureContent = news.pictureContent {
                imageView.image = UIImage(data: pictureContent)
            }
            contentView.loadHTMLString(Utilities.embedInHTMLStructure(news.title, content: news.content), baseURL: nil)
        }
    }
    
    func showActivityViewController() {
        if let news = selectedNews {
            let content = Utilities.embedInHTMLStructure(news.title, content: news.content)
            let activityViewController = UIActivityViewController(activityItems: [content], applicationActivities: nil)
            self.presentViewController(activityViewController, animated: true, completion: nil)
        }
    }
}
