import Foundation

class OpeningHours {
    var id: Int
    var weekday: String
    var fromTime: String
    var untilTime: String
    
    init(id: Int, weekday: String, fromTime: String, untilTime: String) {
        self.id = id
        self.weekday = weekday
        self.fromTime = fromTime
        self.untilTime = untilTime
    }
    
    class func convertToOpeningHours(result: NSArray) -> [OpeningHours] {
        var openingHours = [OpeningHours]()
        for entry in result {
            let id = entry["id"] as! Int
            let weekday = entry["weekday"] as! String
            let fromTime = entry["fromTime"] as! String
            let untilTime = entry["untilTime"] as! String
            openingHours.append(OpeningHours(id: id, weekday: weekday, fromTime: fromTime, untilTime: untilTime))
        }
        return openingHours
    }
}