import Foundation
import CoreLocation

class Address {
    var id: Int
    var street: String
    var postalCode: Int
    var city: String
    var location: CLLocation
    
    init(id: Int, street: String, postalCode: Int, city: String, latitude: Double, longitude: Double) {
        self.id = id
        self.street = street
        self.postalCode = postalCode
        self.city = city
        self.location = CLLocation(latitude: latitude, longitude: longitude)
    }
    
    class func convertToAddress(result: NSDictionary) -> Address{
        let id = result["id"] as! Int
        let street = result["street"] as! String
        let postalCode = result["postalCode"] as! Int
        let city = result["city"] as! String
        let latitude = result["latitude"] as! Double
        let longitude = result["longitude"] as! Double
        return Address(id: id, street: street, postalCode: postalCode, city: city, latitude: latitude, longitude: longitude)
    }
}